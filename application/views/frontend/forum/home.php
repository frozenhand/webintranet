<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head kt-portlet__space-x">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <?= $forum->judul ?>
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <p class="">
                    <span>Oleh:</span>
                    <span class="kt-font-info"><?= $forum->sumber ?></span>
                    <span>Tanggal:</span>
                    <span class="kt-font-info"><?= indonesian_date(date("Y-m-d", strtotime($forum->tanggal_forum))) ?></span>
                </p>
                <p>
                    <?= $forum->isi ?>
                </p>
                <div class="kt-widget3 mt-5">
                    <table id="list-data">
                        <thead>
                            <th>Komentar</th>
                        </thead>
                        <tbody id="forum-data"></tbody>
                    </table>
                </div>
            </div>
            <form action="" class="kt-form" id="post-komentar" method="POST">
                <input type="hidden" name="id_forum" id="id_forum" value="<?= $id_forum?>" style="display:none">
                <div class="kt-portlet__body">
                        <div class="form-group form-group-last">
                            <label for="komentar">Posting Komentar</label>
                            <textarea class="form-control" id="komentar" name="komentar" rows="3" placeholder="Tulis komentar disini"></textarea>
                        </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

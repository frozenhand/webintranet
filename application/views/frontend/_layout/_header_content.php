<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
            ESDM Office </h3>
        <span class="kt-subheader__separator kt-hidden"></span>
        <div class="kt-subheader__breadcrumbs">
            <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a href="<?= base_url() ?>/frontend/home" class="kt-subheader__breadcrumbs-link">
                Dashboard </a>
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <div class="kt-subheader__wrapper">
            <?php foreach ($menu_portal as $key) { ?>
                <a href="<?= $key->uri ?>" class="btn kt-subheader__btn-secondary">
                <?php if (!is_null($key->file_icon)) { ?>
                    <img alt="Logo" style="width: 25px; padding: 2px;" src="<?= base_url().'/upload/file_icon/'.$key->file_icon ?>">
                <?php } else if (!is_null($key->icon_metronic)) {
                    echo $key->icon_metronic;
                    // <img alt="Logo" style="width: 25px; padding: 2px;" src="<?= base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic">
                } ?>
                    <span class="kt-menu__link-text"><?= $key->menu_portal ?></span>
                </a>
            <?php } ?>
        </div>
    </div>
</div>

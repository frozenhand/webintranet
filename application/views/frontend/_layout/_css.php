<!--begin::Page Vendors Styles(used by this page) -->
<!-- <link href="<?php echo base_url() ?>resources/user/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url() ?>resources/user/css/demo1/pages/general/pricing/pricing-1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>resources/user/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->

<!--begin::Global Theme Styles(used by all pages) -->
<link href="<?php echo base_url() ?>resources/user/vendors/global/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>resources/user/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles -->

<!--begin::Layout Skins(used by all pages) -->
<link href="<?php echo base_url() ?>resources/user/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo base_url() ?>resources/user/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" /> -->
<link href="<?php echo base_url() ?>resources/user/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>resources/user/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>resources/user/css/demo1/custom.css" rel="stylesheet" type="text/css" />

<!-- datatable  -->
<link href="<?= base_url(); ?>resources/css/datatables.bundle.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url() ?>resources/user/css/demo1/font-awesome.min.css" rel="stylesheet" />

<!--end::Layout Skins -->
<link rel="shortcut icon" href="<?php echo base_url() ?>resources/user/media/logos/favicon.ico" />
<style type="text/css">
    .kt-widget.kt-widget--user-profile-3 .kt-widget__top .kt-widget__content .kt-widget__subhead a {
        display: block;
        padding-right: 0px;
    }
    select{
        font-family: fontAwesome
    }
</style>

<script src="<?php echo base_url() ?>resources/user/js/demo1/webfont.js" type="text/javascript"></script>
<script type="text/javascript">
    // begin::Fonts
    WebFont.load({
        google: {
            "families": ["DM Sans:300,400,500,600,700", "Roboto:300,400,500,600,700"]
        },
        active: function () {
            sessionStorage.fonts = true;
        }
    });
    // end::Fonts
</script>

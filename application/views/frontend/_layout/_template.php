<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>

	<!--begin::Base Path (base relative path for assets of this page) -->
	<base href="../">

	<!--end::Base Path -->
	<title>ESDM | Dashboard</title>

    <!-- meta -->
    <?= @$_meta; ?>

    <!-- css --> 
    <?= @$_css; ?>

</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize">
	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
    <?= @$_header_mobile ?>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->
            <?= @$_aside; ?>
            <!-- end:: Aside -->

			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<!-- begin:: Header -->
                <?= @$_header ?>
				<!-- end:: Header -->
				
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
					<!-- begin:: Content Head -->
                    <?= @$_header_content ?>
					<!-- end:: Content Head -->

					<!-- begin:: Content -->
					<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

						<!--Begin::Dashboard 1-->

						<!--Begin::Section-->
                        <?= @$_content ?>
						<!--End::Section-->

						<!--End::Dashboard 1-->
					</div>
					<!-- end:: Content -->
				</div>

				<!-- begin:: Footer -->
                <?= @$_footer ?>
				<!-- end:: Footer -->

			</div>
		</div>
	</div>
	<!-- end:: Page -->

	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="modal-pengumuman" tabindex="-1" role="dialog" aria-labelledby="modal-pengumuman-title" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal-pengumuman-title">Pengumuman</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<p class="text-center">
						<img src="<?php echo base_url() ?>resources/user/lp2p.jpg" class="d-inline-block">
					</p>
					<p>
						segera laporkan pajak-pajak pribadi Anda paling lambat 13 September 2019 ke Biro SDM unduh formulir di laman <a href="https://bit.ly/x123reiugjrlwef" target="_blank" rel="noreferrer nofollow">https://bit.ly/x123reiugjrlwef</a>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>

    <?= @$_js ?>
</body>
<!-- end::Body -->

</html>

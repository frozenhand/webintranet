<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?php echo base_url() ?>resources/user/vendors/global/vendors.bundle.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>resources/user/js/demo1/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="<?php echo base_url() ?>resources/user/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
<!-- <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script> -->
<!-- <script src="<?php echo base_url() ?>resources/user/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script> -->
<!--end::Page Vendors -->

<!-- datatable  -->
<script src="<?= base_url(); ?>resources/js/datatables.bundle.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>resources/user/js/demo1/pages/dashboard.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>resources/user/js/demo1/custom.js" type="text/javascript"></script>

<!--begin::Page Scripts(used by this page) -->
<script type="text/javascript">
    // $(function(){
    //     $('#modal-pengumuman').modal('show');
    // });

    // begin::Global Config(global config for global JS sciprts)
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
    // end::Global Config

    // begin:: calendar
    $(document).ready(function() {
        $("#kt_calendar").fullCalendar({
            isRTL: KTUtil.isRTL(),
            header:
                {
                    left: "prev,next today",
                    center: "title",
                    right: "month,agendaWeek,agendaDay,listWeek"
                },
            editable: !0,
            eventLimit: !0,
            navLinks: !0,
            defaultDate: moment(),
            events: function(start, end,timezone, callback) {
                $.get('<?= base_url('frontend/home/getData') ?>', {
                    start: start.unix(),
                    end: end.unix()
                }, function (res) {
                    var events = [];
                    $.each(res, function (index, value) {
                        // var className = '';
                        // switch (value.type) {
                        // 	case 'tna':
                        // 		className = 'm-fc-event--light m-fc-event--solid-success';
                        // 		break;
                        // 	case 'rpi':
                        // 		className = 'm-fc-event--light m-fc-event--solid-danger';
                        // 		break;
                        // 	case 'training':
                        // 		className = 'm-fc-event--light m-fc-event--solid-accent';
                        // 		break;
                        // 	case 'bimbingan':
                        // 		className = 'm-fc-event--light m-fc-event--solid-primary';
                        // 		break;
                        // 	case 'pkl':
                        // 		className = 'm-fc-event--light m-fc-event--solid-info';
                        // 		break;
                        // 	case 'visit':
                        // 		className = 'm-fc-event--light m-fc-event--solid-info';
                        // 		break;
                        // 	case 'penelitian':
                        // 		className = 'm-fc-event--light m-fc-event--solid-metal';
                        // 		break;
                        // 	case 'recycle':
                        // 		className = 'm-fc-event--light m-fc-event--solid-warning';
                        // 		break;
                        // 	case 'pendidikan_formal':
                        // 		className = 'm-fc-event--light m-fc-event--solid-accent';
                        // 		break;
                        // }
                        events.push({
                            title: value.judul,
                            start: moment(value.tanggal_mulai),
                            end: moment(value.tanggal_selesai),
                            description: value.deskripsi
                            // className: className,
                            // idEvent: value.id,
                        });
                    });

                    callback(events);
                }, 'json');
            },
            eventRender: function(event, element) {
                if (element.hasClass('fc-day-grid-event')) {
                    element.data('content', event.description);
                    element.data('placement', 'top');
                    KTApp.initPopover(element);
                } else if (element.hasClass('fc-time-grid-event')) {
                    element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
                } else if (element.find('.fc-list-item-title').lenght !== 0) {
                    element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
                }
            }
            // eventClick: function(data) {

            // 	$.ajax({
            // 		method: "GET",
            // 		url: "<?php 
                // echo base_url('FrontendLMS/PersonalCalendar/getDetail/');
            ?>" + data.typeEvent + '/' + data.idEvent,
            // 	})
            // 	.done(function(result) {
            // 		$('#tempat-modal').html(result);
            // 		sakaScrollable();
            // 		$('#calendar-detail').modal('show');
            // 	})

            // 	return false;
            // }
        });
    });
    // end:: calendar

    $(document).ready(function() {
        $(".lihat-forum").click(function() {
            var id = $(this).attr("data-id");
            $.get('<?php echo base_url('frontend/forum/isi/'); ?>'+id, function(data) {
			    $('#kt_content').html('');
                $('#kt_content').html(data);
		    });
        })
    });
</script>
<!--end::Page Scripts -->

<?php
    // load js per page 
    if ($Page == 'Forum') {
        include './resources/js/frontend/ajax_forum.php';
    } elseif ($Page == 'Berita') {
        include './resources/js/frontend/ajax_berita.php';
    }
?>
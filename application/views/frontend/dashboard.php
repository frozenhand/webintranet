<div class="row">

	<div class="col-md-4">

		<div class="kt-portlet kt-portlet--fit kt-portlet--head-noborder">
			<div class="kt-portlet__head kt-portlet__space-x">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
							height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect id="bound" x="0" y="0" width="24" height="24"></rect>
								<path
									d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z"
									id="Combined-Shape" fill="#000000" opacity="0.3"></path>
								<path
									d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z"
									id="Combined-Shape" fill="#000000"></path>
							</g>
						</svg>
					</span>
					<h3 class="kt-portlet__head-title">
						Status Layanan Perizinan
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body kt-portlet__body--fluid">
				<div class="kt-widget20 user-stats">
					<div class="kt-widget20__content kt-portlet__space-x">
						<span class="kt-widget20__number kt-font-brand">5</span>
						<span class="kt-widget20__desc">On Track</span>
					</div>
					<div class="kt-widget20__content">
						<span class="kt-widget20__number kt-font-warning">2</span>
						<span class="kt-widget20__desc">At Risk</span>
					</div>
					<div class="kt-widget20__content kt-portlet__space-x">
						<span class="kt-widget20__number kt-font-danger">3</span>
						<span class="kt-widget20__desc">Overdue</span>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet kt-portlet--fit kt-portlet--head-noborder">
			<div class="kt-portlet__head kt-portlet__space-x">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
							height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect id="bound" x="0" y="0" width="24" height="24"></rect>
								<path
									d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z"
									id="Combined-Shape" fill="#000000" opacity="0.3"></path>
								<path
									d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z"
									id="Combined-Shape" fill="#000000"></path>
							</g>
						</svg>
					</span>
					<h3 class="kt-portlet__head-title">
						Surat
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body kt-portlet__body--fluid">
				<div class="kt-widget20 user-stats">
					<div class="kt-widget20__content kt-portlet__space-x">
						<span class="kt-widget20__number kt-font-brand">10</span>
						<span class="kt-widget20__desc">Surat Masuk/Disposisi</span>
					</div>
					<div class="kt-widget20__content">
						<span class="kt-widget20__number kt-font-brand">3</span>
						<span class="kt-widget20__desc">Konsep Surat Keluar</span>
					</div>
					<div class="kt-widget20__content kt-portlet__space-x">
						<span class="kt-widget20__number kt-font-danger">4</span>
						<span class="kt-widget20__desc">Belum direspon</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body" style="padding: 0;">
				<div class="kt-pricing-1">
					<div class="kt-pricing-1__items row" style="padding: 0;">
						<div class="kt-pricing-1__item col-lg-3">
							<div class="kt-pricing-1__visual">
								<div class="kt-pricing-1__hexagon2"></div>
								<span class="kt-pricing-1__icon kt-font-brand"><i class="fa flaticon-graphic-2"></i></span>
							</div>
							<span class="kt-pricing-1__price" style="font-size: 3rem; margin-top: 20rem;">90</span>
							<h2 class="kt-pricing-1__subtitle">e-Kinerja Individu</h2>
						</div>
						<div class="kt-pricing-1__item col-lg-3">
							<div class="kt-pricing-1__visual">
								<div class="kt-pricing-1__hexagon2"></div>
								<span class="kt-pricing-1__icon kt-font-success"><i
										class="fa flaticon-dashboard"></i></span>
							</div>
							<span class="kt-pricing-1__price">97</span>
							<h2 class="kt-pricing-1__subtitle">Kinerja Organisasi</h2>
						</div>
						<div class="kt-pricing-1__item col-lg-3">
							<div class="kt-pricing-1__visual">
								<div class="kt-pricing-1__hexagon2"></div>
								<span class="kt-pricing-1__icon kt-font-danger"><i class="fa flaticon-coins"></i></span>
							</div>
							<span class="kt-pricing-1__price">95<span class="kt-pricing-1__label">%</span></span>
							<h2 class="kt-pricing-1__subtitle">Realisasi Belanja</h2>
						</div>
						<div class="kt-pricing-1__item col-lg-3">
							<div class="kt-pricing-1__visual">
								<div class="kt-pricing-1__hexagon2"></div>
								<span class="kt-pricing-1__icon kt-font-warning"><i class="fa flaticon-diagram"></i></span>
							</div>
							<span class="kt-pricing-1__price">92<span class="kt-pricing-1__label">%</span></span>
							<h2 class="kt-pricing-1__subtitle">Realisasi PNBP</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<div class="kt-portlet kt-portlet--height-fluid kt-portlet--head-noborder">
			<div class="kt-portlet__head kt-portlet__space-x">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
							height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect id="bound" x="0" y="0" width="24" height="24" />
								<path
									d="M5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z"
									id="Combined-Shape" fill="#000000" />
							</g>
						</svg>
					</span>
					<h3 class="kt-portlet__head-title">
						Email
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="kt-widget20">
					<div class="kt-widget20__content" style="">
						<span class="kt-widget20__number kt-font-danger">10</span>
						<span class="kt-widget20__desc">Belum dibaca</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="kt-portlet kt-portlet--height-fluid kt-portlet--head-noborder">
			<div class="kt-portlet__head kt-portlet__space-x">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon kt-pulse kt-pulse--danger">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
							height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect id="bound" x="0" y="0" width="24" height="24" />
								<path
									d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
									id="Combined-Shape" fill="#000000" opacity="0.3" />
								<path
									d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z"
									id="check-path" fill="#000000" />
								<path
									d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
									id="Combined-Shape" fill="#000000" />
							</g>
						</svg>
						<span class="kt-pulse__ring" style="top: -5px; left: -8px;"></span>
					</span>
					<h3 class="kt-portlet__head-title">
						Ketidakhadiran
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body kt-portlet__body--fluid">
				<div class="kt-widget20 user-stats">
					<div class="kt-widget20__content">
						<span class="kt-widget20__number kt-font-danger">12</span>
						<span class="kt-widget20__desc">Jumlah tidak absen dalam hari</span>
					</div>
					<div class="kt-widget20__content">
						<span class="kt-widget20__number kt-font-danger">12</span>
						<span class="kt-widget20__desc">Jumlah pengajuan ketidakhadiran</span>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col-md-6">

		<!--Begin::Portlet-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Agenda Hari Ini
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="dropdown dropdown-inline">
						<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="flaticon-more-1"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">

							<!--begin::Nav-->
							<ul class="kt-nav">
								<li class="kt-nav__head">
									Export Options
									<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="Click to learn more..."></i>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-link"></i>
										<span class="kt-nav__link-text">Settings</span>
									</a>
								</li>
								<li class="kt-nav__item">
									<a href="#" class="kt-nav__link">
										<i class="kt-nav__link-icon flaticon2-new-email"></i>
										<span class="kt-nav__link-text">Support</span>
									</a>
								</li>
								<li class="kt-nav__separator"></li>
								<li class="kt-nav__foot">
									<a class="btn btn-label-danger btn-bold btn-sm" href="#">PDF</a>
									<a class="btn btn-label-info btn-bold btn-sm" href="#">Excel</a>
								</li>
							</ul>

							<!--end::Nav-->
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet__body">

				<!--Begin::Timeline 3 -->
				<div class="kt-timeline-v2">
					<div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
						<?php foreach ($event as $key) { ?>
							<div class="kt-timeline-v2__item">
								<span class="kt-timeline-v2__item-time">
									<?= date('H:i', strtotime($key->tanggal_mulai)) ?>
								</span>
								<div class="kt-timeline-v2__item-cricle">
									<?php
									$warna = array(
										"kt-font-brand", 
										"kt-font-light", 
										"kt-font-dark", 
										"kt-font-primary", 
										"kt-font-success", 
										"kt-font-info", 
										"kt-font-warning", 
										"kt-font-danger");
									$pilih_warna = $warna[array_rand($warna)];
									?>
									<i class="fa fa-genderless <?= $pilih_warna ?>"></i>
								</div>
								<div class="kt-timeline-v2__item-text  kt-padding-top-5">
									<?= $key->judul ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>

				<!--End::Timeline 3 -->
			</div>
		</div>

		<!--End::Portlet-->
	</div>
	<div class="col-md-6">

		<!--Begin::Portlet-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Kalender
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div id="kt_calendar"></div>
			</div>
		</div>
	</div>

	<!--End::Section-->

	<!-- <div class="col-md-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__space-x">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Kurva-S Pusat Data Dan Teknologi Informasi Kesdm
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<canvas id="canvas"></canvas>
			</div>
		</div>
	</div> -->


	<h3 class="col-md-12 mb-3">
		Sosial Media
	</h3>
	<div class="col-md-4">

		<!--Begin::Portlet-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Facebook
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<!-- uncomment code dibawah untuk mengaktifkan widget Facebook -->
				<iframe title="Facebook Page" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fkesdm&tabs=timeline&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=661144383905425&width=288&height=490" width="288" height="490" style="width: 100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true">
				</iframe>
			</div>
		</div>
	</div>

	<div class="col-md-4">

		<!--Begin::Portlet-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Twitter
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<!-- uncomment code dibawah untuk mengaktifkan widget Twitter -->
				<a title="Twitter Timeline" class="twitter-timeline" data-height="550" href="https://twitter.com/KementerianESDM">Tweets by KementerianESDM</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>
		</div>
	</div>

	<div class="col-md-4">

		<!--Begin::Portlet-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Instagram
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<!-- uncomment code dibawah untuk mengaktifkan widget Instagram -->
				<a title="Instagram" href="https://instawidget.net/v/user/kesdm" id="link-c661212969b756ab95b1ea7bcc8828bb92e8d9e8ce350b11dc8e6df07126c0fe">@kesdm</a>
				<script src="https://instawidget.net/js/instawidget.js?u=c661212969b756ab95b1ea7bcc8828bb92e8d9e8ce350b11dc8e6df07126c0fe&width=100%&height=550" type="text/javascript"></script>
			</div>
		</div>
	</div>

	<h3 class="col-md-12 mb-3">
		Berita Terbaru
	</h3>
	<?php foreach ($berita as $key) { ?>
		<div class="col-md-4">

			<!--begin:: Widgets/Blog-->
			<div class="kt-portlet kt-portlet--height-fluid kt-widget19">
				<div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
					<div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url(<?php echo base_url() ?>upload/thumbnail/<?= $key->thumbnail ?>)">
						<h3 class="kt-widget19__title kt-font-light">
							<?= $key->judul ?>
						</h3>
						<div class="kt-widget19__shadow"></div>
						<div class="kt-widget19__labels">
							<a href="#" class="btn btn-label-light-o2 btn-bold btn-sm ">Berita</a>
						</div>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="kt-widget19__wrapper">
						<div class="kt-widget19__content">
							<!-- <div class="kt-widget19__userpic">
								<img src="<?php echo base_url() ?>resources/user/media//users/user1.jpg" alt="">
							</div> -->
							<div class="kt-widget19__info" style="padding-left: 0">
								<a href="#" class="kt-widget19__username">
									<?= $key->sumber ?>
								</a>
								<span class="kt-widget19__time">
									<?= indonesian_date(date("Y-m-d", strtotime($key->tanggal_berita))) ?>
								</span>
							</div>
							<div class="kt-widget19__stats">
								<span class="kt-widget19__number kt-font-brand">
									<?= $key->JmlKomen ?>
								</span>
								<a href="#" class="kt-widget19__comment">
									komentar
								</a>
							</div>
						</div>
						<div class="kt-widget19__text">
							<?= $key->highlight ?>
						</div>
					</div>
					<div class="kt-widget19__action">
						<a href="<?= base_url()."frontend/berita/isi/".$key->id ?>" class="btn btn-sm btn-label-brand btn-bold" target="_blank">Baca Berita...</a>
					</div>
				</div>
			</div>

			<!--end:: Widgets/Blog-->
		</div>
	<?php } ?>

	<div class="col-md-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__head kt-portlet__space-x">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Forum
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<div class="kt-widget5">
					<?php foreach ($forum as $key) { ?>
						<div class="kt-widget5__item">
							<div class="kt-widget5__content">
								<div class="kt-widget5__pic">
									<img class="kt-widget7__img" src="<?= base_url().'upload/forum/'.$key->thumbnail ?>" alt="">
								</div>
								<div class="kt-widget5__section">
									<a class="kt-widget5__title">
										<?= $key->judul ?>
									</a>
									<p class="kt-widget5__desc">
										<?= $key->highlight ?>...
									</p>
									<div class="kt-widget5__info">
										<span>Oleh:</span>
										<span class="kt-font-info"><?= $key->sumber ?></span>
										<span>Kategori:</span>
										<span class="kt-font-info"><?= $key->kategori ?></span>
										<span>Tanggal:</span>
										<span class="kt-font-info"><?= indonesian_date(date("Y-m-d", strtotime($key->tanggal_forum))) ?></span>
										<!-- <button type="button" class="btn btn-sm btn-label-info lihat-forum" data-id="<?= $key->id ?>">Baca</button> -->
										<a href="<?= base_url()."frontend/forum/isi/".$key->id ?>" class="btn btn-sm btn-label-info" target="_blank">Baca</a>
									</div>
								</div>
							</div>
							<div class="kt-widget5__content">
								<div class="kt-widget5__stats">
									<span class="kt-widget5__number"><?= $key->dilihat ?></span>
									<span class="kt-widget5__sales">dilihat</span>
								</div>
								<div class="kt-widget5__stats">
									<span class="kt-widget5__number"><?= $key->JmlKomen ?></span>
									<span class="kt-widget5__votes">komentar</span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

</div>

<?php
$week = 0;
foreach ($komentar as $key) {
?>
    <tr>
        <td>
            <div class="kt-widget3__item">
                <div class="kt-widget3__header">
                    <div class="kt-widget3__user-img">
                        <img class="kt-widget3__img" src="./assets/media/users/user1.jpg" alt="">
                    </div>
                    <div class="kt-widget3__info">
                        <a href="#" class="kt-widget3__username">
                            Melania Trump
                        </a><br>
                        <span class="kt-widget3__time">
                            <?php
                            $date1 = date_create($key->tanggal_komentar);
                            $date2 = date_create();
                            $interval = date_diff($date1, $date2);
                            if ($interval->y != 0) {
                                if ($interval->y > 1) echo $interval->y.' years ago';
                                else echo $interval->y.' year ago';
                            } else if ($interval->m != 0) {
                                if ($interval->m > 1) echo $interval->m.' months ago';
                                else echo $interval->m.' month ago';
                            } else if ($interval->d != 0) {
                                $week = intval($interval->d / 7);
                                if ($week > 0) {
                                    if ($week > 1) {
                                        echo $week.' weeks ago';
                                    } else if ($week == 1) {
                                        echo $week.' week ago';
                                    }
                                } else {
                                    if ($interval->d == 1) echo $interval->d.' day ago';
                                    else echo $interval->d.' days ago';
                                }
                            } else if ($interval->h != 0) {
                                if ($interval->h > 1) echo $interval->h.' hours ago';
                                else echo $interval->h.' hour ago';
                            } else if ($interval->i != 0) {
                                if ($interval->i > 1) echo $interval->i.' minutes ago';
                                else echo $interval->i.' minute ago';
                            } else if ($interval->s != 0) {
                                echo $interval->i.' min ago';
                            }
                            ?>
                        </span>
                    </div>
                    <span class="kt-widget3__status">
                        <!-- <a href="#" class="btn btn-sm btn-label-brand btn-bold">
                            Reply
                        </a> -->
                    </span>
                </div>
                <div class="kt-widget3__body">
                    <p class="kt-widget3__text">
                        <?= $key->komentar ?>
                    </p>
                </div>
            </div>
        </td>
    </tr>
<?php } ?>

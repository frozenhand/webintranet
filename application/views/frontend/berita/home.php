<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
            <div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
                <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url(<?php echo base_url() ?>upload/thumbnail/<?= $berita->thumbnail ?>)">
                    <h3 class="kt-widget19__title kt-font-light">
                        <?= $berita->judul ?>
                    </h3>
                    <div class="kt-widget19__shadow"></div>
                    <div class="kt-widget19__labels">
                        <a href="#" class="btn btn-label-light-o2 btn-bold btn-sm ">Berita</a>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget19__wrapper">
                        <div class="kt-widget19__content">
                            <div class="kt-widget19__userpic">
                                <img src="./assets/media//users/user1.jpg" alt="">
                            </div>
                            <div class="kt-widget19__info">
                                <a href="#" class="kt-widget19__username">
                                    <?= $berita->sumber ?>
                                </a>
                                <span class="kt-widget19__time">
                                    <?= indonesian_date(date("Y-m-d", strtotime($berita->tanggal_berita))) ?>
                                </span>
                            </div>
                            <div class="kt-widget19__stats">
                                <span class="kt-widget19__number kt-font-brand" id="total-komentar">
                                    <?= $totalkomentar ?>
                                </span>
                                <a href="#" class="kt-widget19__comment">
                                    Komentar
                                </a>
                            </div>
                        </div>
                        <div class="kt-widget19__text">
                            <?= $berita->isi ?>
                        </div>
                    </div>
                    <!-- <div class="kt-widget19__action">
                        <a href="#" class="btn btn-sm btn-label-brand btn-bold"><i class="fa fa-plus"></i>Komentar</a>
                    </div> -->
                    <div class="kt-widget3 mt-5">
                        <table id="list-data">
                            <thead>
                                <th>Komentar</th>
                            </thead>
                            <tbody id="berita-data"></tbody>
                        </table>
                    </div>
                </div>
                <form action="" class="kt-form" id="post-komentar" method="POST">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="id_berita" id="id_berita" value="<?= $id_berita?>" style="display:none">
                        <div class="form-group form-group-last">
                            <label for="komentar">Komentar</label>
                            <textarea class="form-control" id="komentar" name="komentar" rows="3" placeholder="Tulis komentar disini"></textarea>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

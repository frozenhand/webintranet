<section class="content-header">
	<h1> 
		Dashboard
		<small>Minerba</small>
	</h1>
	<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">Dashboard</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<a href="<?php echo base_url().'backend/agenda_kegiatan/agenda_kegiatan'?>">
					<span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>
				</a>
				<div class="info-box-content">
				<span class="info-box-text"><?php echo format_datetime(date('Y-m-d'));?></span>
				<span id="clockbox" class="info-box-number"></span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<a href="https://statcounter.com/login/">
					<span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
				</a>
				<div class="info-box-content">
				<span class="info-box-text">Statcounter</span>
				<span class="info-box-number">
				</span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<a href="https://analytics.google.com/analytics/web/">
					<span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
				</a>
				<div class="info-box-content">
				<span class="info-box-text">Google Analytic</span>
				<span class="info-box-number">
				</span>
				</div>
			</div>
		</div>
	</div>
</section>
  <script type="application/javascript">
      function GetClock(){
      var d=new Date();
      var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

      if(nhour==0){ap=" AM";nhour=12;}
      else if(nhour<12){ap=" AM";}
      else if(nhour==12){ap=" PM";}
      else if(nhour>12){ap=" PM";nhour-=12;}

      if(nmin<=9) nmin="0"+nmin;
      if(nsec<=9) nsec="0"+nsec;

      document.getElementById('clockbox').innerHTML=""+nhour+":"+nmin+":"+nsec+ap+"";
      }

      window.onload=function(){
      GetClock();
      setInterval(GetClock,1000);
      
      }
  </script>
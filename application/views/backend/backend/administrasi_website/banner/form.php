<?php
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- /.row -->
<section class="content-header">
    <h1><?php echo $page_title; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url().$this->module; ?>"><?php echo $page_title; ?></a></li>
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <div class="box-body">
                        <?php
                        if ( !empty($errMsg) ) {
                            ?>
                            <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Tema</label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="tema" value="<?php echo empty($detail['tema']) ? NULL : $detail['tema']; ?>" max_length="100">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-3">Banner</label>
                           <?php
                           $current_banner = empty($detail['banner']) ? NULL : $detail['banner'];
                           if ( !empty($current_banner) ) {
                            if ( file_exists('./resources/frontend/images/'.$current_banner) ) {
                                ?>
                                <div class="col-xs-9">
                                  <img src="<?php echo base_url().'resources/frontend/images/'.$current_banner; ?>" style="max-width: 100%;">
                                </div>
                                <?php
                            }
                           }
                           ?>
                           <div class="col-md-3 col-sm-3 col-xs-3">
                               <input type="file" class="form-control" name="banner" value="<?php echo empty($detail['banner']) ? NULL : $detail['banner']; ?>" max_length="100">
                           </div>
                           <div class="col-md-4 col-sm-4 col-xs-4">
                               <span><small class="label bg-yellow"><em>* Ukuran File : 1079 x 120 pixel</em></small></span>
                               <span><small class="label bg-red"><em>* Maks Ukuran File: 30MB</em></small></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-3">Banner En</label>
                           <?php
                           $current_banner_en = empty($detail['banner_en']) ? NULL : $detail['banner_en'];
                           if ( !empty($current_banner_en) ) {
                            if ( file_exists('./resources/frontend/images/'.$current_banner_en) ) {
                                ?>
                                <div class="col-xs-9">
                                  <img src="<?php echo base_url().'resources/frontend/images/'.$current_banner_en; ?>" style="max-width: 100%;">
                                </div>
                                <?php
                            }
                           }
                           ?>
                           <div class="col-md-3 col-sm-3 col-xs-3">
                               <input type="file" class="form-control" name="banner_en" value="<?php echo empty($detail['banner_en']) ? NULL : $detail['banner_en']; ?>" max_length="100">
                           </div>
                           <div class="col-md-4 col-sm-4 col-xs-4">
                               <span><small class="label bg-yellow"><em>* File Size : 1079 x 120 pixel</em></small></span>
                               <span><small class="label bg-red"><em>* Max File Size: 30MB</em></small></span>
                           </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="<?php echo $url_back; ?>">Kembali</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $(".textarea").wysihtml5();
        $('.datepicker').datepicker({
            autoclose: true
        });
        $('.cmb_select2').select2({
            theme: 'bootstrap'
        });
    });
</script>
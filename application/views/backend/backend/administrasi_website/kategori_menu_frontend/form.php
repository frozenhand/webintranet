<?php
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- /.row -->
<section class="content-header">
    <h1><?php echo $page_title; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url().$this->module; ?>"><?php echo $page_title; ?></a></li>
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <div class="box-body">
                        <?php
                        if ( !empty($errMsg) ) {
                            ?>
                            <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-6">Parent</label>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                              <select class="form-control cmb_select2" id="id_parent" name="id_parent">
                                  <option value=""> - Pilih Parent - </option>
                                  <?php
                                  $list_parent = $this->global_model->get_list('tbl_kategori_menu_frontend','id_parent IS NULL AND is_show=1');
                                  foreach ( $list_parent as $key ) {
                                      $selected = $key->id_kategori_menu == (empty($detail['id_parent']) ? NULL : $detail['id_parent']) ? 'selected' : NULL;
                                      ?>
                                      <option value="<?php echo $key->id_kategori_menu; ?>" <?php echo $selected; ?>><?php echo $key->kategori_menu;?></option>
                                      <?php
                                  }
                                  ?>       
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori Menu <span class="required">*</span></label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="kategori_menu" value="<?php echo empty($detail['kategori_menu']) ? NULL : $detail['kategori_menu']; ?>" max_length="255" required>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori Menu En <span class="required">*</span></label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="kategori_menu_en" value="<?php echo empty($detail['kategori_menu_en']) ? NULL : $detail['kategori_menu_en']; ?>" max_length="255" required>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Uri </label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="uri" value="<?php echo empty($detail['uri']) ? NULL : $detail['uri']; ?>" max_length="255">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-6">Urutan <span class="required">*</span></label>
                           <div class="col-md-3 col-sm-3 col-xs-6">
                               <input type="number" class="form-control is_numeric" name="urutan" value="<?php echo empty($detail['urutan']) ? NULL : $detail['urutan']; ?>" required>
                           </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="<?php echo $url_back; ?>">Kembali</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $(".textarea").wysihtml5();
        $('.datepicker').datepicker({
            autoclose: true
        });
        $('.cmb_select2').select2({
            theme: 'bootstrap'
        });
    });
</script>
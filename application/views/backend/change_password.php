<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ganti Password
            </div>
            <div class="panel-body">
                <?php
                if ( !empty($msg_change_password) ) {
                    ?>
                    <div class="alert alert-info">
                        <?php echo $msg_change_password; ?>
                    </div>
                    <?php
                }
                ?>
                <form id="form_change_password" class="form-horizontal" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <input type="hidden" name="id" value="<?php echo $this->session->userdata('id_admin'); ?>">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-4">Password Lama</label>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <input class="form-control" name="password_lama" type="password" maxlength="20" required>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <em style="color:red">*Min. 8 - Maks. 20 karakter </em>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-4">Password Baru</label>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <input class="form-control char_password" name="password_baru" type="password" maxlength="20" required>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <em style="color:red">*Min. 8 - Maks. 20 karakter </em>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-4">Konfirmasi Password</label>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <input class="form-control char_password" name="ulangi_password" type="password" maxlength="20" required>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <em style="color:red">*Min. 8 - Maks. 20 karakter </em>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" name="btn_simpan" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function () {
    $(".textarea").wysihtml5();
    $('.datepicker').datepicker({
        autoclose: true
    });
    $("#datepicker").datepicker( {
        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years"
    });
    $('.char_password').unbind('keyup change input paste').bind('keyup change input paste',function(e){
        var $this = $(this);
        var val = $this.val();
        var valLength = val.length;
        var maxCount = $this.attr('maxlength');
        if(valLength>maxCount){
            $this.val($this.val().substring(0,maxCount));
        }
    });
    $('#form_change_password').validate({
      rules: {
        password_lama: {
          required: true,
          minlength: 8
        },
        password_baru: {
          required: true,
          minlength: 8
        },
        ulangi_password: {
          required: true,
          minlength: 8
        }
      }
    }); 
});
</script>

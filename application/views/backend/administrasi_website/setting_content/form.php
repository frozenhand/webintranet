<?php
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- /.row -->
<section class="content-header">
    <h1><?php echo $page_title; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url().$this->module; ?>"><?php echo $page_title; ?></a></li>
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <div class="box-body">
                        <?php
                        if ( !empty($errMsg) ) {
                            ?>
                            <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Konten</label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="jenis_konten" value="<?php echo empty($detail['jenis_konten']) ? NULL : $detail['jenis_konten']; ?>" max_length="200">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Value</label>
                           <?php 
                           $id_setting = isset($detail['id_setting']) ? $detail['id_setting'] : NULL;
                           if( $id_setting != 8) { ?>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <textarea class="textarea" name="value" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo empty($detail['value']) ? NULL : $detail['value']; ?></textarea>
                           </div>
                           <?php } else { ?>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                              <?php
                              $images = empty($detail['value']) ? NULL : $detail['value'];
                              if ( !empty($images) ) {
                                $filepath = 'resources/frontend/images/'.$images;
                                if ( file_exists('./'.$filepath) ) {
                                  ?>
                                  <img src="<?php echo base_url().$filepath ?>" style="max-width:100%;">
                                  <?php
                                }
                              }
                              ?>
                              <input type="file" class="form-control" name="value" value="<?php echo empty($detail['value']) ? NULL : $detail['value']; ?>" max_length="200">
                           </div>
                           <?php } ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="<?php echo $url_back; ?>">Kembali</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker({
            autoclose: true
        });
        $('.cmb_select2').select2({
            theme: 'bootstrap'
        });
    });
</script>
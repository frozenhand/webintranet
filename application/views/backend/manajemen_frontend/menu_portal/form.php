<?php
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- /.row -->


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<style>
	select{
		font-family: fontAwesome
	}
</style>

<section class="content-header">
    <h1><?php echo $page_title; ?></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Home</a></li>
        <li><a href="<?php echo base_url().$this->module; ?>"><?php echo $page_title; ?></a></li>
        <li class="active"><?php echo $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $title; ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name() ?>" value="<?php echo $this->security->get_csrf_hash() ?>">
                    <div class="box-body">
                        <?php
                        if ( !empty($errMsg) ) {
                            ?>
                            <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Icon / File <span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <?php
                                $tampilkan_menu = isset($detail['file_or_icon']) ? $detail['file_or_icon'] : 2; 
                                ?>
                                <label class="radio-inline"><input type="radio" name="file_or_icon" value="1" <?php echo $tampilkan_menu == 1 ? 'checked' : NULL; ?>>Icon</label>
                                <label class="radio-inline"><input type="radio" name="file_or_icon" value="0" <?php echo $tampilkan_menu == 0 ? 'checked' : NULL; ?>>File</label>
                            </div>
                        </div>
                        <div id="file_or_icon">
                            <?php if ($tampilkan_menu == 1) { ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Icon</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        <select class="form-control cmb_select2" name="id_icon">
                                            <option value=""> - Pilih Icon - </option>
                                            <?php
                                            $list_icon = $this->global_model->get_list('tbl_icon_metronic');
                                            foreach ( $list_icon as $dt ) {
                                                $selected = $dt->id_icon_metronic == (empty($detail['id_icon']) ? NULL : $detail['id_icon']) ? 'selected' : NULL;
                                                ?>
                                                <option value="<?php echo $dt->id_icon_metronic; ?>" <?php echo $selected; ?>><?php echo $dt->icon_metronic; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } else if ($tampilkan_menu == 0) { ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">File</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="file" class="form-control" name="file_icon">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Menu Portal <span class="required">*</span></label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="menu_portal" value="<?php echo empty($detail['menu_portal']) ? NULL : $detail['menu_portal']; ?>" max_length="255" required>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Cname</label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="cname" value="<?php echo empty($detail['cname']) ? NULL : $detail['cname']; ?>" max_length="100">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-12">Uri <span class="required">*</span></label>
                           <div class="col-md-9 col-sm-9 col-xs-12">
                               <input type="text" class="form-control" name="uri" value="<?php echo empty($detail['uri']) ? NULL : $detail['uri']; ?>" max_length="255" required>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-md-3 col-sm-3 col-xs-6">Urutan <span class="required">*</span></label>
                           <div class="col-md-3 col-sm-3 col-xs-6">
                               <input type="number" class="form-control is_integer" name="urutan" value="<?php echo empty($detail['urutan']) ? NULL : $detail['urutan']; ?>" required>
                           </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="<?php echo $url_back; ?>">Kembali</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function formatState (state) {
        console.log(state.text);
        if (state.text == ' - Pilih Icon - ') return state.text;
        return '<img src="<?= base_url() ?>/resources/user/media/icons/svg/'+state.text+'"></i> '+state.text;
    };

    $(function () {
        $(".textarea").wysihtml5();
        $('.datepicker').datepicker({
            autoclose: true
        });
        $('.cmb_select2').select2({
            theme: 'bootstrap',
            templateResult: formatState,
            escapeMarkup : function(markup) {
                return markup;
            }
        });
    });

    $('input:radio[name="file_or_icon"]').change(
    function(){
        $("#file_or_icon").html("");

        if ($('input:radio[name="file_or_icon"]').is(':checked')) {
            var file_or_icon = $(this).val();

            if (file_or_icon == 0) {
                var isi_html = '<div class="form-group">'+
                                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">File</label>'+
                                        '<div class="col-md-9 col-sm-9 col-xs-12">'+
                                            '<input type="file" class="form-control" name="file_icon">'+
                                    '</div>'+
                                '</div>';
                $("#file_or_icon").append(isi_html);
            } else if (file_or_icon == 1) {
                var isi_html = '<div class="form-group">'+
                                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Icon</label>'+
                                        '<div class="col-md-9 col-sm-9 col-xs-12">'+
                                        '<select class="form-control cmb_select2" name="id_icon">'+
                                            '<option value=""> - Pilih Icon - </option>'+
                                            '<?php
                                            $list_icon = $this->global_model->get_list('tbl_icon_metronic');
                                            foreach ( $list_icon as $dt ) {
                                                $selected = $dt->id_icon_metronic == (empty($detail['id_icon']) ? NULL : $detail['id_icon']) ? 'selected' : NULL;
                                                ?>'+
                                                '<option value="<?php echo $dt->id_icon_metronic; ?>" <?php echo $selected; ?>><?php echo $dt->icon_metronic; ?></option>'+
                                            '<?php } ?>'+
                                        '</select>'+
                                    '</div>'+
                                '</div>';
                $("#file_or_icon").append(isi_html);
            }
        }
    });
</script>
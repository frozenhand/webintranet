<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        // $this->load->helper('captcha');
    }

    public function index() {
        // $username = $this->session->userdata('username');
        // $id_role = $this->session->userdata('id_role');

        /*$vals = array(
            'word'          => random_word(5, false, true,true),
            'img_path'      => './captcha/',
            'img_url'       => base_url().'captcha/',
            'font_path'     => './system/fonts/texb.ttf',
            'img_width'     => '150',
            'img_height'    => 33,
            'expiration'    => 7200,
            'word_length'   => 5,
            'font_size'     => 16,
            'colors'        => array(
                'background' => array(200, 200, 200),
                'border' => array(0, 0, 0),
                'text' => array(0, 0, 0),
                'grid' => array(255, 255, 20)
            )
        );
        $cap = create_captcha($vals);*/
        $err = $this->session->flashdata('login_message');
        $data = array(
            //'captcha_image' => $cap['image'],
            'err'           => $err
        );

        // if(!empty($username) && !empty($id_role) ){
        //     redirect(base_url().'home');
        // }

        if ( $_POST ) {
            //$current_captcha = $this->session->userdata('mycaptcha');
            $this->validation(/*$current_captcha*/);
        } else {
            //$this->session->set_userdata('mycaptcha', $cap['word']);
            $this->load->view('frontend/login/login_view', $data);
        }
    }

    private function validation(/*$mycaptcha*/){
        // $username = $this->input->post('txt_username');
        // $password = $this->input->post('txt_password');
        //$captcha  = $this->input->post('txt_captcha');
        // $this->form_validation->set_rules('txt_username','username','required');
        // $this->form_validation->set_rules('txt_password','password','required');
        // $site_key = $this->config->item('captcha_site_key');
        // $secret_key = $this->config->item('captcha_secret_key');
        // if(isset($_POST['g-recaptcha-response'])) {
            // $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response='.$_POST['g-recaptcha-response'];
            // $response = @file_get_contents($api_url);
            // $data = json_decode($response, true);
            // if($this->form_validation->run() === FALSE){
            //     $msg = "Username atau Password tidak valid";
            //     $this->session->set_flashdata('login_message', $msg);
            //     redirect(base_url().'frontend/login');
            // } else if ( !($data['success']) ) {
            //     $msg = "Kode captcha tidak valid";
            //     $this->session->set_flashdata('login_message', $msg);
            //     redirect(base_url().'frontend/login');
            // } else{
                // $result = $this->login_model->validation($username, $password);

                // if($result){
                //     $this->session->set_userdata('id_admin', $result->id_admin);
                //     $this->session->set_userdata('username', $result->username);
                //     $this->session->set_userdata('nama', $result->nama);
                //     $this->session->set_userdata('id_role', $result->id_role);
                //     $this->session->set_userdata('role', $result->role);
                //     $this->session->set_userdata('is_super_admin', $result->is_super_admin);
                //     $last_page = $this->session->userdata('last_page');
                //     if ( !empty($last_page) ) {
                //         $this->session->unset_userdata('last_page');
                //         redirect($last_page);
                //     } else {
                //         redirect(base_url().'frontend/home');
                //     }
                // } else {
                //     $msg = "Username atau Password tidak valid";
                //     $this->session->set_flashdata('login_message', $msg);
                //     redirect(base_url().'frontend/login');
                // }
            // }
        // } else {
        //     $msg = "Kode captcha tidak valid";
        //         $this->session->set_flashdata('login_message', $msg);
        //         redirect(base_url().'frontend/login');
        // }
        redirect(base_url().'frontend/home');
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url().'frontend/login');
    }       
}


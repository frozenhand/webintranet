<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
class forum extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('frontend/dashboard_model', 'app_model');
        $this->load->model('frontend/forum_model');
        $this->load->model('global_model');
    }

	public function index() {
        // wajib dibawa di setiap fungsi yang menampilkan halaman
        $data['kategoriMenu'] = $this->forum_model->list_menu();
        $data['Menu']		= $this->forum_model->sub_menu();
        $data['sidebar']    = $this->forum_model->sidebar();
        $data['menu_portal']= $this->forum_model->menu_portal();
        // ------------------------------------------------------

		$this->templatefrontend->views('frontend/forum/home', $data);
    }

	public function isi($id) {
        // increment total yang melihat forum 
        $totalViewer = $this->forum_model->get_by_id($id)->dilihat;
        $dataUpdate = array('dilihat' => ($totalViewer+1));
        $updateData = $this->forum_model->update_total_viewer_by_id($id, $dataUpdate);

        // var_dump($dataUpdate);die();

        // wajib dibawa di setiap fungsi yang menampilkan halaman
        $data['kategoriMenu'] = $this->app_model->list_menu();
        $data['Menu']		= $this->app_model->sub_menu();
        $data['sidebar']    = $this->app_model->sidebar();
        $data['menu_portal']= $this->app_model->menu_portal();

        foreach ($data['sidebar'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }

        foreach ($data['menu_portal'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon kt-svg-icon--warning"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }
        // ------------------------------------------------------

        $data['Page']           = 'Forum';
        $data['id_forum']       = $id;
        $data['forum']          = $this->forum_model->get_by_id($id);
        $data['totalkomentar']  = $this->forum_model->get_komentar_by_id($id, 'count');
        $data['komentar']       = $this->forum_model->get_komentar_by_id($id, 'list');

        $this->templatefrontend->views('frontend/forum/home', $data);
		// $this->load->view('frontend/forum/home', $data);
    }

    function tampil($id) {
        $data['komentar']       = $this->forum_model->get_komentar_by_id($id, 'list');
        
		$this->load->view('frontend/forum/datalist', $data);
    }

    function komentar() {
        $data = $this->input->post();
        $tanggal = date('Y-m-d H:i:s');
        $dataInsert = array(
            'id_forum'  => $data['id_forum'],
            'komentar'  => $data['komentar'],
            'tanggal_komentar' => $tanggal
        );

        $hasil = $this->forum_model->insert_komentar($dataInsert);

		echo json_encode($out);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
class home extends CI_Controller {
    function __construct(){
        parent::__construct();

        $this->load->model('frontend/dashboard_model','app_model');
        $this->load->model('global_model');
    }
	public function index()
	{

        // wajib dibawa di setiap fungsi yang menampilkan halaman
        $data['kategoriMenu'] = $this->app_model->list_menu();
        $data['Menu']		= $this->app_model->sub_menu();
        $data['sidebar']    = $this->app_model->sidebar();
        $data['menu_portal']= $this->app_model->menu_portal();
        // ------------------------------------------------------

        $data['Page']       = 'Dashboard';
        $data['berita']     = $this->app_model->berita();
        $data['forum']      = $this->app_model->forum();
        $data['event']      = $this->app_model->event();

        foreach ($data['sidebar'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }

        foreach ($data['menu_portal'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon kt-svg-icon--warning"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }
		$this->templatefrontend->views('frontend/dashboard', $data);
    }

    function change_password() {
        $this->load->model('backend/login_model');
        if ( isset($_POST['btn_simpan']) ) {
            $id_user            = $this->session->userdata('id_admin');
            $password_lama      = $this->input->post('password_lama');
            $password_baru      = $this->input->post('password_baru');
            $ulangi_password    = $this->input->post('ulangi_password');
            $check_password     = $this->login_model->check_password($id_user, $password_lama);

            if ( !$check_password ) {
                $this->session->set_flashdata('msg_change_password', 'Password lama tidak valid');
            } else if ( empty($password_baru) ) {
                $this->session->set_flashdata('msg_change_password', 'Password baru masih kosong');
            } else if ( $password_baru  != $ulangi_password ) {
                $this->session->set_flashdata('msg_change_password', 'Pengulangan tidak valid');
            } else {
                $data_update    = array(
                    'password' => strrev(md5($password_baru))
                );
                $update = $this->login_model->update_user($id_user, $data_update);
                $this->session->set_flashdata('msg_change_password', 'Password berhasil diubah');
            }
            redirect(base_url().'backend/home/change_password');
        }

        $data['msg_change_password']    = $this->session->flashdata('msg_change_password');
        $data['breadcrumb']             = array(
            array(
                'label' => 'Ubah Password',
                'link'  => NULL
            )
        );
        $data['active_breadcrumb']      = 'Ubah Password';

        $this->template->add_js('resources/plugins/validation/jquery.validate.min.js');
        $this->template->add_js('resources/plugins/validation/additional-methods.min.js');
        $this->template->write('title', 'Ubah Password');
        $this->template->write_view('content', 'backend/change_password', $data, true);
        $this->template->render();
    }

    function gapi(){
        $data = array();
        $data['client_id'] = '183671426529-69gfuvj85kprao839cim2svbtk15su85.apps.googleusercontent.com';
        $this->load->view('backend/gapi', $data);
    }

    public function getData() {
        $get = $this->input->get();
        $start = date('Y-m-d', $get['start']);
        $end = date('Y-m-d', $get['end']);
        // var_dump($get.' '.$start.' '.$end);die();

        $calendar = $this->app_model->getCalendar($start, $end);
        echo json_encode($calendar);
    }

}

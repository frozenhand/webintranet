<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
class berita extends CI_Controller {
    function __construct() {
        parent::__construct();

        $this->load->model('frontend/berita_model');
        $this->load->model('frontend/dashboard_model', 'app_model');
        $this->load->model('global_model');
    }

	public function index() {
        // wajib dibawa di setiap fungsi yang menampilkan halaman
        $data['kategoriMenu'] = $this->app_model->list_menu();
        $data['Menu']		= $this->app_model->sub_menu();
        $data['sidebar']    = $this->app_model->sidebar();
        $data['menu_portal']= $this->app_model->menu_portal();

        foreach ($data['sidebar'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }

        foreach ($data['menu_portal'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon kt-svg-icon--warning"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }
        // ------------------------------------------------------

        $data['berita'] = $this->berita_model->get_by_id($id);

		$this->templatefrontend->views('frontend/berita/home', $data);
    }
    
	public function isi($id) {
        // wajib dibawa di setiap fungsi yang menampilkan halaman
        $data['kategoriMenu'] = $this->app_model->list_menu();
        $data['Menu']		= $this->app_model->sub_menu();
        $data['sidebar']    = $this->app_model->sidebar();
        $data['menu_portal']= $this->app_model->menu_portal();

        foreach ($data['sidebar'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }

        foreach ($data['menu_portal'] as $key) {
            if (!is_null($key->icon_metronic)) {
                $color = 'class="kt-svg-icon kt-svg-icon--warning"';
                $icon = str_replace('class=""', $color, read_file(base_url().'/resources/user/media/icons/svg/'.$key->icon_metronic));
            } else {
                $icon = NULL;
            }
            $key->icon_metronic = $icon;
        }
        // ------------------------------------------------------

        $data['Page']           = 'Berita';
        $data['id_berita']      = $id;
        $data['berita']         = $this->berita_model->get_by_id($id);
        $data['totalkomentar']  = $this->berita_model->get_komentar_by_id($id, 'count');
        $data['komentar']       = $this->berita_model->get_komentar_by_id($id, 'list');

		$this->templatefrontend->views('frontend/berita/home', $data);
    }

    function tampil($id) {
        $data['komentar']       = $this->berita_model->get_komentar_by_id($id, 'list');
        // echo '<pre>';
        // var_dump($data['komentar']);
        // echo '</pre>';
        // die();
        
		$this->load->view('frontend/berita/datalist', $data);
    }

    function komentar() {
        $data = $this->input->post();
        $tanggal = date('Y-m-d H:i:s');
        $dataInsert = array(
            'id_berita' => $data['id_berita'],
            'komentar'  => $data['komentar'],
            'tanggal_komentar' => $tanggal
        );

        $hasil = $this->berita_model->insert_komentar($dataInsert);
        $out['totalkomentar']  = $this->berita_model->get_komentar_by_id($data['id_berita'], 'count');

		echo json_encode($out);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
class home extends My_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('backend/dashboard_model','app_model');
    }
	public function index()
	{
        $this->load->model('global_model');

        // $data['user_online'] = get_ol_user();
        $data['kontak'] = $this->global_model->get_count_data('tbl_agenda');
		    $this->template->write_view('content', 'backend/dashboard', $data, true);
        $this->template->render();
    }

    function change_password() {
        $this->load->model('backend/login_model');
        if ( isset($_POST['btn_simpan']) ) {
            $id_user            = $this->session->userdata('id_admin');
            $password_lama      = $this->input->post('password_lama');
            $password_baru      = $this->input->post('password_baru');
            $ulangi_password    = $this->input->post('ulangi_password');

            $check_password     = $this->login_model->check_password($id_user, $password_lama);

            if ( !$check_password ) {
                $this->session->set_flashdata('msg_change_password', 'Password lama tidak valid');
            } else if ( empty($password_baru) ) {
                $this->session->set_flashdata('msg_change_password', 'Password baru masih kosong');
            } else if ( $password_baru  != $ulangi_password ) {
                $this->session->set_flashdata('msg_change_password', 'Pengulangan tidak valid');
            } else {
                $data_update    = array(
                    'password' => strrev(md5($password_baru))
                );
                $update = $this->login_model->update_user($id_user, $data_update);
                $this->session->set_flashdata('msg_change_password', 'Password berhasil diubah');
            }
            redirect(base_url().'backend/home/change_password');
        }

        $data['msg_change_password']    = $this->session->flashdata('msg_change_password');
        $data['breadcrumb']             = array(
            array(
                'label' => 'Ubah Password',
                'link'  => NULL
            )
        );
        $data['active_breadcrumb']      = 'Ubah Password';

        $this->template->add_js('resources/plugins/validation/jquery.validate.min.js');
        $this->template->add_js('resources/plugins/validation/additional-methods.min.js');
        $this->template->write('title', 'Ubah Password');
        $this->template->write_view('content', 'backend/change_password', $data, true);
        $this->template->render();
    }

    function gapi(){
        $data = array();
        $data['client_id'] = '183671426529-69gfuvj85kprao839cim2svbtk15su85.apps.googleusercontent.com';
        $this->load->view('backend/gapi', $data);
    }
}

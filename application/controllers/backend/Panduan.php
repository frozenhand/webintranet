<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panduan extends MY_Controller {

	
	public function index()
	{	
		$data = array();
		$data['page_title'] = "Panduan Penggunaan Website Backend Minerba";
	    $this->template->write_view('content', 'backend/panduan', $data, true);
        $this->template->write('title', 'Panduan Penggunaan Website Backend Minerba');
        $this->template->render();
	}
}

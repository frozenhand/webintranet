<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
class Setting_content extends MY_Controller {
    var $page_title = 'Pengaturan Konten';
    var $column_datatable = array('id_setting', 'jenis_konten','value');
    var $folder         = 'backend/administrasi_website';
    var $module         = '';

    function __construct(){
        parent::__construct();
        $module = $this->folder.'/'.$this->router->fetch_class();
        $this->load->model($this->folder.'/'.'setting_content_model', 'app_model');
        $this->app_model->initialize($module);
        $this->module = $module;
    }

    public function index()
    {
        $module =$this->module;
        $script = '
            var active_id = \'\';
            $(function () {
                var oTable = $("#datatable").DataTable({
                    responsive: {
                        details: {
                            type: "column",
                            target: -1
                        }
                    },
                     columnDefs: [ {
                        className: "control",
                        orderable: false,
                        targets:   -1
                    }],
                    "order": [[ 0, "desc" ]],
                    dom: "Bfrtip",
                    buttons: [
                        '.($this->is_write ? '{ text: "<span class=\'fa fa-plus-circle\'></span> Tambah Data", className: "btn btn-sm btn-primary",
                         action: function ( e, dt, node, config ) {
                            go_add_data();
                        }},' : NULL).'
                        '.($this->is_write ? '{ text: "<span class=\'fa fa-pencil\'></span> Edit Data", className: "btn btn-sm btn-warning",
                         action: function ( e, dt, node, config ) {
                            go_edit_data();
                        }},' : NULL).'
                        '.($this->is_write ? '{ text: "<span class=\'fa fa-refresh\'></span> Tampilkan", className: "btn btn-sm btn-success",
                         action: function ( e, dt, node, config ) {
                            go_show();
                        }},' : NULL).'
                    ],
                    "processing": true,
                    "serverSide": true,
                    "ajax" : {
                        "url": "'.base_url().$module.'/getDataTable",
                        "type": "POST", "data": { \''.$this->security->get_csrf_token_name().'\' : \''.$this->security->get_csrf_hash().'\' }
                    }
                });
                oTable.on( "click", "tr", function () {
                    var objcheck = $(this).children().find(".selectedrow");
                    selectrowtable(objcheck);
                } );
            });
            function selectrowtable(obj) {
                var parentrow = obj.parent().parent();
                if (  active_id != obj.val() ) {
                    $("#datatable tbody tr").removeClass("selected");
                    parentrow.addClass("selected");
                    active_id = obj.val();
                } else {
                        parentrow.removeClass("selected");
                        active_id = "";
                }
            }
            function go_add_data() {
                document.location = "'.base_url().$this->module.'/add";
            }
            function go_edit_data() {
                if ( active_id.length < 1 ) {
                    alert(\'Pilih baris data pada tabel yang ingin diubah!\');
                } else {
                    document.location = "'.base_url().$this->module.'/edit/" + active_id;
                }
            }
            function go_show() {
                if ( active_id.length < 1 ) {
                    alert(\'Pilih baris data pada tabel yang ingin diubah!\');
                } else {
                    document.location = "'.base_url().$this->module.'/show/" + active_id;
                }
            }
            ';
        $data['title'] = $this->page_title;
        $this->template->add_css('resources/plugins/datatables/dataTables.bootstrap.css');
        $this->template->add_css('resources/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css');
        $this->template->add_js('resources/plugins/datatables/jquery.dataTables.min.js');
        $this->template->add_js('resources/plugins/datatables/dataTables.bootstrap.min.js');
        $this->template->add_js('resources/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');
        $this->template->add_js('resources/plugins/datatables/dataTables.buttons.min.js');
        $this->template->add_js('resources/plugins/datatables/buttons.flash.min.js');
        $this->template->add_js('resources/plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js');

        $this->template->add_js($script,'embed');
        $this->template->write('title', $data['title']);
        $this->template->write_view('content', $this->folder.'/setting_content/datatable', $data, true);
        $this->template->render();
    }

    function getDataTable()
    {
        $iDisplayStart = $this->input->get_post('start', true);
        $iDisplayLength = $this->input->get_post('length', true);
        $order = $this->input->get_post('order', true);
        $sSearch = $this->input->get_post('search', true);

        $view = $this->app_model->show_datatable($this->column_datatable, $iDisplayStart,$iDisplayLength,$order,$sSearch);

        echo $view;
    }

    private function valid_form($act = 'add') {
        $this->load->library('form_validation');
        $config = array(
                    array(
                        'field' => 'jenis_konten',
                        'label' => 'Jenis Konten',
                        'rules' => 'max_length[200]'
                    ),
                
                    array(
                        'field' => 'value',
                        'label' => 'Value',
                        'rules' => 'max_length[65535]'
                    ),
                );
        $this->form_validation->set_rules($config);
    }

    function add() {
        $this->load->model('global_model');
        $module = $this->module;
        $data['detail']     = array();
        $data['title']      = "Tambah Data";
        $data['url_back']   = "window.location.href='".base_url().$module."'";
        $errMsg = NULL;

        if($_POST)
        {
            $data_post      = $this->input->post();
            $data['detail'] = $data_post;
            $data_insert    = array();
            $simpan         = true;
            $this->valid_form(strtolower(__FUNCTION__));

            if ( $this->form_validation->run() == FALSE ) {
                $simpan = false;
                $errMsg = '<ul>'.validation_errors('<li>','</li>').'</ul>';
            }

            if ( $simpan ) {
                $data_insert = array(
            'jenis_konten' => strip_tags($this->input->post('jenis_konten')),
                'value' => $this->input->post('value'),
            );

                $insert = $this->app_model->insert_data($data_insert);
                if ( $insert ) {					insert_log('Tambah pengaturan konten');
                    redirect(base_url().$module);
                } else {
                    $errMsg = 'Data gagal disimpan';
                }
            }
        }
        $data['page_title'] = $this->page_title;
        $data['errMsg']     = $errMsg;
        $this->template->add_css('resources/plugins/select2/select2.min.css');
        $this->template->add_css('resources/plugins/select2/select2-bootstrap.min.css');
        $this->template->add_js('resources/plugins/select2/placeholders.jquery.min.js');
        $this->template->add_js('resources/plugins/select2/select2.min.js');
        $this->template->write('title', 'Tambah '.$this->page_title);
        $this->template->write_view('content', $this->folder.'/setting_content/form', $data, true);
        $this->template->render();
    }

    function edit($id = 0) {
        $this->load->model('global_model');
        $module = $this->module;
        $data['detail'] = $this->app_model->get_by_id($id);
        if ( !$data['detail'] ) {
            show_404();
            return;
        }
        $data['title'] = "Edit Data";
        $data['url_back'] = "window.location.href='".base_url().$this->module."'";
        $errMsg = NULL;

        if($_POST)
        {
            $data_post      = $this->input->post();
            $data['detail'] = $data_post;
            $simpan         = true;

            $this->valid_form('edit');

            if ( $this->form_validation->run() == FALSE ) {
                $simpan = false;
                $errMsg = '<ul>'.validation_errors('<li>','</li>').'</ul>';
            }

            if ( $simpan ) {
                if($id == 8){
                    $this->load->library('upload');
                    $this->load->library('image_lib');
                    $tmp_name = $_FILES['value']['tmp_name'];
                    if ( !empty($tmp_name) ) {
                        $extension_expl = explode('.', $_FILES['value']['name']);
                        $extension = $extension_expl[count($extension_expl)-1];
                        $file_name = 'opening_'.date("YmdHis");

                        $upload_conf = array(
                            'upload_path'       => 'resources/frontend/images/',
                            'allowed_types'     => 'gif|jpg|png',
                            'max_size'          => '30000',
                            'file_name'         => $file_name.'.'.$extension,
                        );
                        $this->upload->initialize($upload_conf);
                        if ( !$this->upload->do_upload('value') ) {
                            $simpan = false;
                            $errMsg = $this->upload->display_errors('<li>','</li>');
                        } else {
                            $upload_data = $this->upload->data();
                            $size = getimagesize($upload_data['full_path']);
                            $maxWidth = 750;
                            $maxHeight = 450;
                            if ($size[0] > $maxWidth || $size[1] > $maxHeight){
                                $resize_real = array(
                                'source_image'  => $upload_data['full_path'], 
                                'width'         => 750,
                                );
                                $this->image_lib->initialize($resize_real);
                                if ( ! $this->image_lib->resize()){
                                    $errMsg = $this->image_lib->display_errors('<li>','</li>');
                                }
                                else{
                                    $errMsg = $upload_data;
                                }
                            }
                            $old_file = $data['detail']['value'];
                            $old_filepath = './resources/frontend/images/'.$old_file;
                            if ( file_exists($old_filepath) ) 
                                unlink($old_filepath);
                        }
                        $data_update = array(    
                            'jenis_konten' => strip_tags($this->input->post('jenis_konten')),
                            'value' => $upload_conf['file_name'],
                        );
                    }
                }else{
                    $data_update = array(    
                        'jenis_konten' => strip_tags($this->input->post('jenis_konten')),
                        'value' => $this->input->post('value'),
                    );
                }
                
                $update = $this->app_model->update_data($id, $data_update);
                if ( $update ) {					insert_log('Ubah pengaturan konten');
                    redirect(base_url().$module);
                } else {
                    $errMsg = 'Data gagal disimpan';
                }
            }
        }
        $data['page_title'] = $this->page_title;
        $data['id'] = $id;
        $data['errMsg'] = $errMsg;
        $this->template->add_css('resources/plugins/select2/select2.min.css');
        $this->template->add_css('resources/plugins/select2/select2-bootstrap.min.css');
        $this->template->add_js('resources/plugins/select2/placeholders.jquery.min.js');
        $this->template->add_js('resources/plugins/select2/select2.min.js');
        $this->template->write('title', 'Edit '.$this->page_title);
        $this->template->write_view('content', $this->folder.'/setting_content/form', $data, true);
        $this->template->render();
    }

    function show($id){
        $module = $this->module;
        $this->load->model('global_model');
        $show = $this->global_model->get_by_id_array('tbl_setting_content','id_setting',$id);
        $status = $show['is_show'] == '0' ? '1' : '0';
        $data_update = array(
                'is_show' => $status
            );
        $update = $this->global_model->update_data('tbl_setting_content', 'id_setting', $id,$data_update);
        if ( $update ) {			insert_log('Ubah status pengaturan konten');
                    redirect(base_url().$module);
        } else {
            echo 'Data gagal disimpan';
        }

    }
}

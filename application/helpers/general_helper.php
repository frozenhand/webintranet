<?php
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 */
function get_data_web($name = '') {
    $output = '';
    $CI = &get_instance();
    $CI->db->select('value')
           ->from('setting_web')
           ->where('LOWER(variable_name)', strtolower($name));
    $query = $CI->db->get();
    if ( is_object($query) ) {
        $data = $query->row();
        if ( is_object($data) )
            $output = $data->value;
    }
    return $output;
}
function translate($key = '') {
    $output = '';
    $CI = &get_instance();
    $lang = $CI->session->userdata('bahasa');
    switch($lang) {
        default:
            $bahasa = 'translate';
            break;
        case '_en':
            $bahasa = 'translate_en';
            break;
    }
    $CI->db->select($bahasa)
        ->from('tbl_translate')
        ->where('id', strtolower($key));
    $query = $CI->db->get();
    if ( is_object($query) ) {
        $data = $query->row();
        if ( is_object($data) )
            $output = $data->$bahasa;
    }
    return $output;
}
function word_shorter($text = '', $length = 100) {
    $new_string = '';
    if ( !empty($text) ) {
        $new_string = substr(strip_tags($text), 0, $length);
        if ( strlen($text) > $length ) {
            $new_string .= ' ...';
        }
    }
    return $new_string;
}
function set_seo($text = "") {
    $text = str_replace('-', ' ', $text);
    $text = preg_replace("/[[:blank:]]+/"," ",$text);
    $result = str_replace('--','-',str_replace(' ','-', preg_replace('/[^a-zA-Z0-9\s]/', '-', strtolower(trim($text)))));
    if ( $result[strlen($result)-1] == '-' ) {
        $result = substr($result, 0, strlen($result)-1);
    }
    return $result;
}
function describe_seo($text = "") {
    $output = str_replace('-',' ',str_replace('_',' ',$text));
    return $output;
}
function convert_field_to_string($field = '') {
    $label = $field;
    if ( !empty($field) ) {
        $label = str_replace('_',' ',$label);
        $label = ucwords($label);
    }
    return $label;
}
function clear_text($text = '') {
    $val = strip_tags($text, '<blockquote><a><table><span><ul><ol><b><i><h1><h2><h3><h4><h5><div><hr>');
    return $val;
}
function get_field_data($table_name = '') {
    $CI = &get_instance();
    $sql = "SELECT COLUMN_NAME AS name,
            DATA_TYPE AS type,
            CHARACTER_MAXIMUM_LENGTH AS max_length,
            CASE WHEN UPPER(IS_NULLABLE) = 'NO' THEN 0 ELSE 1 END is_null,
            CASE WHEN UPPER(COLUMN_KEY) = 'PRI' THEN 1 ELSE 0 END primary_key,
            COLUMN_COMMENT AS label
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_SCHEMA = '".$CI->db->database."'
            AND TABLE_NAME= '".$table_name."'";
    $query = $CI->db->query($sql);
    if ( is_object($query) ) {
        $data = $query->result();
        return $data;
    }
    return false;
}
function format_datetime($datetime = '') {
    $result = '';
    if ( !empty($datetime) ) {
        $expl_space     = explode(' ', $datetime);
        $tanggal        = $expl_space[0];
        $expl_tanggal   = explode('-', $tanggal);
        if ( count($expl_tanggal) > 2 ) {
            $result = $expl_tanggal[2] . ' ' . get_nama_bulan($expl_tanggal[1]) . ' ' . $expl_tanggal[0];
        }
    }

    return $result;
}
function short_datetime($datetime = '') {
    $result = '';
    if ( !empty($datetime) ) {
        $expl_space     = explode(' ', $datetime);
        $tanggal        = $expl_space[0];
        $expl_tanggal   = explode('-', $tanggal);
        if ( count($expl_tanggal) > 2 ) {
            $result = $expl_tanggal[2] . ' ' . get_nm_bulan($expl_tanggal[1]) . ' ' . $expl_tanggal[0];
        }
    }

    return $result;
}
function get_nm_bulan($kode_bulan = '') {
    $kode_bulan = (int)$kode_bulan;
    $result     = '';

    switch($kode_bulan) {
        default: $result = 'Jan'; break;case 2: $result = 'Feb'; break;
        case 3: $result = 'Mar'; break;case 4: $result = 'Apr'; break;
        case 5: $result = 'Mei'; break;case 6: $result = 'Jun'; break;
        case 7: $result = 'Jul'; break;case 8: $result = 'Ags'; break;
        case 9: $result = 'Sep'; break;case 10: $result = 'Okt'; break;
        case 11: $result = 'Nov'; break;case 12: $result = 'Des'; break;
    }
    return $result;
}
function get_nama_bulan($kode_bulan = '') {
    $kode_bulan = (int)$kode_bulan;
    $result     = '';

    switch($kode_bulan) {
        default: $result = 'Januari'; break;case 2: $result = 'Februari'; break;
        case 3: $result = 'Maret'; break;case 4: $result = 'April'; break;
        case 5: $result = 'Mei'; break;case 6: $result = 'Juni'; break;
        case 7: $result = 'Juli'; break;case 8: $result = 'Agustus'; break;
        case 9: $result = 'September'; break;case 10: $result = 'Oktober'; break;
        case 11: $result = 'November'; break;case 12: $result = 'Desember'; break;
    }
    return $result;
}
function GetBetween($var1="",$var2="",$pool){
    if ( !empty($var1) )
        $temp1 = strpos($pool,$var1)+strlen($var1);
    else
        $temp1 = 0;
    $result = substr($pool,$temp1,strlen($pool));

    $dd = 0;
    if ( !empty($var2) )
        $dd=strpos($result,$var2);
    if($dd == 0){
        $dd = strlen($result);
    }

    return substr($result,0,$dd);
}
function random_word($length = 5, $is_upper = false, $is_lower = true, $is_number=false) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if($is_number){
     $chars = "0123456789";
    } else {

    if ( $is_upper )
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if ( $is_lower )
        $chars = "abcdefghijklmnopqrstuvwxyz";
    }
    $i = 0;
    $string = "";
    while ($i < $length) {
        $string .= $chars{mt_rand(0,strlen($chars)-1)};
        $i++;
    }
    return $string;
}
function get_list_kategori_menu($is_backend) {
    $CI = &get_instance();
    $CI->db->from('tbl_kategori_menu')->where('is_backend', $is_backend);
    $query = $CI->db->get();
    if ( is_object($query) ) {
        return $query->result();
    }
    return array();
}

function get_list_menu($id_kategori_menu = 0,$is_backend) {
    $CI = &get_instance();
    $id_role = $CI->session->userdata('id_role');
    $is_super_admin = $CI->session->userdata('is_super_admin');
    $where = "1=1 AND is_backend = ".$is_backend;
    if ( empty($id_kategori_menu) )
        $where .= " AND id_kategori_menu IS NULL";
    else
        $where .= " AND id_kategori_menu = ".$id_kategori_menu;
    if ( $is_super_admin == 1 ) {
        $CI->db->select('A.*')
            ->from('tbl_menu A')
            ->where($where)
            ->order_by('urutan');
    } else {

        $where .= " AND B.is_read = 1";
        $CI->db->select('A.*')
            ->from('tbl_menu A')
            ->join('tbl_hak_akses B', 'A.id_menu = B.id_menu AND B.id_role = '.$id_role)
            ->where($where)
            ->order_by('urutan');
    }
    $query = $CI->db->get();
    if ( is_object($query) ) {
        return $query->result();
    }
    return array();
}

function show_hide_banner($session){
    $CI = &get_instance();
    $sql="SELECT * FROM  tbl_user_online WHERE session_id ='$session'";
    //echo $sql;
    $query = $CI->db->query($sql);
    $count = $query->num_rows();
    return $count;
}

function setting_content($val = ''){
    $CI = &get_instance();
    $CI->db->select('value')
        ->from('tbl_setting_content')
        ->where('id_setting', $val);
    $query = $CI->db->get();
    if ( is_object($query) ) {
        $data = $query->row();
        if ( is_object($data) )
            $output = $data->value;
    }
    return $output;
}

function check_counter(){
    $CI = &get_instance();
    $sql = "SELECT value_task as jml FROM tbl_user_online WHERE variable_task = 'counter_website' LIMIT 1";
    $query = $CI->db->query($sql);
    if(is_object($query)){
        return $query->row()->jml;
    }
    return false;
}

function update_counter(){
    $CI = &get_instance();
    $sql = "UPDATE tbl_user_online SET value_task = value_task+1 WHERE variable_task = 'counter_website'";
    $query = $CI->db->query($sql);
    if(is_object($query)){
        if(is_object($query->row()))
        return $query->row_array();
    }
    return false;
}

function get_list_menu_frontend($id_kategori_menu = NULL) {
    $CI = &get_instance();
    $CI->load->model('global_model');
    
    $where = "1=1";
    if ( empty($id_kategori_menu) )
        $where .= " AND id_kategori_menu IS NULL";
    else
        $where .= " AND id_kategori_menu = ".$id_kategori_menu;
   
    $CI->db->select('A.*')
        ->from('tbl_menu_frontend A')
        ->where($where)
        ->where('tampilkan_menu', 1)
        ->order_by('urutan');
    $query = $CI->db->get();
    if ( is_object($query) ) {
        $result = $query->result();
        return $result;
    }
    return array();
}

function generate_kategori_menu($id_parent = 0, $level = 1) {
    $CI = &get_instance();
    // $controller = $CI->router->fetch_class();
    $controller = $_SERVER['REQUEST_URI'];
    // var_dump($controller);
    $suffix = '';
    $active_bahasa = $CI->session->userdata('bahasa');
    if ( $active_bahasa == '_en' ) 
        $suffix = $active_bahasa;
    $column_category = 'kategori_menu'.$suffix;
    $column_menu = 'menu'.$suffix;
    if ( $level <= 2 ) {
        if ( empty($id_parent) ){
            $CI->db->where('id_parent IS NULL and is_show=1');
        } else {
            $CI->db->where('id_parent', $id_parent);
        }            

        $CI->db->order_by('urutan');
        $query = $CI->db->get('tbl_kategori_menu_frontend');
        if ( is_object($query) ) {
            $result = $query->result();
            $string_menu = NULL;
            foreach ($result as $res) {
                $cat_active = '';
                $list_menu = get_list_menu_frontend($res->id_kategori_menu);
                $sub_kategori_menu = generate_kategori_menu($res->id_kategori_menu, ($level+1));
                $cat_icon = '<i class="fa fa-arrow"></i>';
                if ( count($list_menu) > 0 || !empty($res->uri)) {

                    $temp_string_menu = NULL;
                    $temp_string_menu .= '<ul style="text-align:left" class="sub">';

                    $have_active = false;
                    foreach ($list_menu as $menu) {
                        $class_active = NULL;
                        if ( '/'.$menu->uri == $controller ) {
                            $class_active = ' class="active"';
                            $have_active = true;
                        }
                        $icon = '<i class="fa fa-th-list"></i>';
                        if($menu->cname =='humas'){
                            $temp_string_menu .= '<li'.$class_active.'><a href="http://humas.minerba.esdm.go.id/" target="_blank">'.$menu->$column_menu.'</a></li>';
                        }
                        elseif($menu->cname =='simkp'){
                            $temp_string_menu .= '<li'.$class_active.'><a href="http://simkp.minerba.esdm.go.id/" target="_blank">'.$menu->$column_menu.'</a></li>';
                        }
                        elseif($menu->cname =='minerba_one_map'){
                            $temp_string_menu .= '<li'.$class_active.'><a href="https://momi.minerba.esdm.go.id/gisportal/home/" target="_blank">'.$menu->$column_menu.'</a></li>';
                        }
                        elseif($menu->cname =='sim_lingkungan'){
                            $temp_string_menu .= '<li'.$class_active.'><a href="http://simlingkungan.minerba.esdm.go.id/sim/" target="_blank">'.$menu->$column_menu.'</a></li>';
                        }
                        elseif($menu->link_file != 0){ //link file
                            $temp_string_menu .= '<li'.$class_active.'><a style="color:#FFFFFF" href="'.base_url().'show/show_pdf?link_file='.$menu->link_file.'">'.$menu->$column_menu.'</a></li>';
                        }
                        elseif($menu->rte != 0){ //halaman
                            $temp_string_menu .= '<li'.$class_active.'><a style="color:#FFFFFF" href="'.base_url().'show/show_halaman?halaman='.$menu->rte.'">'.$menu->$column_menu.'</a></li>';
                        }
                        elseif ($menu->link_file == 0 && $menu->rte == 0 && !empty($menu->cname)) { //galerry dll
                            $temp_string_menu .= '<li'.$class_active.'><a href="'.base_url().$menu->uri.'" target="_blank">'.$menu->$column_menu.'</a></li>';
							//ke server
                        }
                        else{ //link aplikasi
                            $temp_string_menu .= '<li'.$class_active.'><a style="color:#FFFFFF" href="'.$menu->uri.'" target="_blank">'.$menu->$column_menu.'</a></li>';
                        }                        
                    }
                    $class_active_cat = $have_active || preg_match('/class\=\"active\"/', $sub_kategori_menu) ? ' class="active"' : NULL;
                    $string_menu .= '<li'.$class_active_cat.'>';
                    if(!empty($res->uri)){
                        $string_menu .= '<a href="'.base_url().$res->uri.'">'.$cat_icon.' '.strtoupper($res->$column_category).'</span></a>';
                    }else{
                        $string_menu .= '<a href="#">'.$cat_icon.' '.strtoupper($res->$column_category).' <span class="fa fa-arrow"></span></a>';
                    }                    
                    $string_menu .= $temp_string_menu;
                    $string_menu .= $sub_kategori_menu;
                    $string_menu .= '</ul>';
                    $string_menu .= '</li>';
                } 
                else if ( !empty($sub_kategori_menu) ) {
                    $temp_string_menu = NULL;
                    $temp_string_menu .= '<ul class="clear" style="color: #fff000">';
                    $temp_string_menu .= $sub_kategori_menu;
                    $class_active_cat = preg_match('/class\=\"active\"/', $sub_kategori_menu) ? ' class="active"' : NULL;
                    $string_menu .= '<li'.$class_active_cat.'>';
                    $string_menu .= '<a href="#">'.$cat_icon.' '.strtoupper($res->$column_category).' <span class="fa fa-arrow"></span></a>';
                    $string_menu .= $temp_string_menu;
                    $string_menu .= '</ul>';
                    $string_menu .= '</li>';
                }
            }
            return $string_menu;
        }
    }
    return NULL;
}

// adnan
function generate_menu() {
    $CI = &get_instance();
    $suffix = '';
    $active_bahasa = $CI->session->userdata('bahasa');
    if ( $active_bahasa == '_en' ) 
        $suffix = $active_bahasa;
    $column_menu = 'menu'.$suffix;
    $list_menu = get_list_menu_frontend();
    // $controller = $CI->router->fetch_class();
    $controller = $_SERVER['REQUEST_URI'];
    // var_dump($controller);exit();
    $string_menu = '<ul class="clear" style="color: #fff000">';

    $home_active = $controller == 'home' ? 'class="active"' : NULL;
    $text_home = $active_bahasa == '_en' ? 'HOME' : 'BERANDA';
    $string_menu .= '<li '.$home_active.'>
                    <a href="'.base_url().'">'.$text_home.'</a>
                </li>';
    
    foreach ($list_menu as $menu) {
        $class_active = '/'.$controller == $menu->uri ? ' class="active"' : NULL;
        $icon = '<i class="fa fa-th-list"></i>';
        $string_menu .= '<li'.$class_active.'>';
        $string_menu .= '<a href="'.base_url().$menu->uri.'">'.$menu->$column_menu.'</a>';
        $string_menu .= '</li>';
    }
    $string_menu .=  generate_kategori_menu();
    $string_menu .= '</ul>';
    return $string_menu;
}

function insert_log($proses = NULL, $link = NULL) {	$CI = &get_instance();	if ( !empty($proses) ) {		$dt_insert = array(			'proses'	=> $proses,			'link'		=> $link,			'username'	=> $CI->session->userdata('username'),			'created'	=> date('Y-m-d H:i:s')		);		$CI->db->insert('tbl_log', $dt_insert);	}}


function show_file_columns($filename = '', $path = '') {
    if ( !empty($filename) ) {
        $expl = explode('.', $filename);
        $extension = $expl[count($expl)-1];
        if ( in_array(strtolower($extension), array('jpg','jpeg','gif','png','ico','bmp')) ) {
            return '<img class="img" src="'.base_url().$path.$filename.'" style="max-width: 100%">';
        } else {
           return '<a href="javascript:Popup(\''.base_url().$path.$filename.'\')"><i class="fa fa-download"></i> Lihat File</a> <script>var stile = "top=10, left=10, width=600, height=800 status=no, menubar=no, toolbar=no scrollbar=no"; function Popup(apri) { window.open(apri, "", stile); }</script>';
            // return '<a href="'.base_url().$path.$filename.'" target="_blank">Lihat File</a>';
        }
    }
    return NULL;
}
function indonesian_date($tanggal){
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>
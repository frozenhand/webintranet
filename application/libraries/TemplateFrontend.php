<?php
	class TemplateFrontend {
		protected $_ci;

		function __construct() {
			$this->_ci = &get_instance(); //Untuk Memanggil function load, dll dari CI. ex: $this->load, $this->model, dll
		}

		function views($template = NULL, $data = NULL) {
			if ($template != NULL) {
				// head
				$data['_meta'] 					= $this->_ci->load->view('frontend/_layout/_meta', $data, TRUE);
				$data['_css'] 					= $this->_ci->load->view('frontend/_layout/_css', $data, TRUE);
				
				// Header
				$data['_header_mobile'] 	    = $this->_ci->load->view('frontend/_layout/_header_mobile', $data, TRUE);
				$data['_header'] 				= $this->_ci->load->view('frontend/_layout/_header', $data, TRUE);
				$data['_header_content'] 		= $this->_ci->load->view('frontend/_layout/_header_content', $data, TRUE);
				
				//Sidebar
				$data['_aside'] 				= $this->_ci->load->view('frontend/_layout/_aside', $data, TRUE);
				$data['_aside_menu'] 			= $this->_ci->load->view('frontend/_layout/_aside_menu', $data, TRUE);
				
				//Content
				$data['_content'] 			    = $this->_ci->load->view($template, $data, TRUE);
				
				//Footer
				$data['_footer'] 				= $this->_ci->load->view('frontend/_layout/_footer', $data, TRUE);
				
				//JS
				$data['_js'] 					= $this->_ci->load->view('frontend/_layout/_js', $data, TRUE);

				echo $data['_template'] 		= $this->_ci->load->view('frontend/_layout/_template', $data, TRUE);
			}
		}
	}
?>
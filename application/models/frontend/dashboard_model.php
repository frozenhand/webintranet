<?php
/**
 * Pembuat : Arif Kurniawan
 * Contact : arif.kurniawan86@gmail.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class dashboard_model extends CI_Model
{
    function __construct(){
        parent::__construct();
    }

    function list_menu(){
		$this->db->select('kategori_menu');
		$this->db->from('tbl_kategori_menu');
		$data = $this->db->get();
		return $data->row();
    }
    function sub_menu(){
		$this->db->select('*');
		$this->db->from('tbl_menu');
		$this->db->join('tbl_kategori_menu', 'tbl_menu.id_menu = tbl_kategori_menu.id_kategori_menu');
		$data = $this->db->get();
		return $data->result();
    }
    function sidebar() {
		$data = $this->db->select('a.sidebar, b.icon_metronic, a.cname, a.uri')
			->from('tbl_sidebar a')
			->join('tbl_icon_metronic b', 'a.id_icon = b.id_icon_metronic')
			->order_by('urutan', 'ASC')
			->get();

		return $data->result();
    }
    function menu_portal() {
		$data = $this->db->select('a.menu_portal, b.icon_metronic, a.cname, a.uri, a.file_icon')
			->from('tbl_menu_portal a')
			->join('tbl_icon_metronic b', 'a.id_icon = b.id_icon_metronic', 'left')
			->order_by('urutan', 'ASC')
			->get();

		return $data->result();
    }
    function berita() {
		$data = $this->db->select("berita.id, judul, tanggal_berita, isi, thumbnail, sumber, highlight, count(komen.id) as JmlKomen")
			->from('tbl_berita as berita')
			->join("tbl_berita_komentar as komen", "berita.id = komen.id_berita", "left")
			->where('highlight IS NOT NULL', NULL, FALSE)
			->group_by("berita.id, judul, tanggal_berita, isi, thumbnail, sumber, highlight")
			->order_by('tanggal_berita', 'DESC')
			->limit(3)
			->get();

		return $data->result();
    }
    function forum() {
		$data = $this->db->select("forum.id, judul, tanggal_forum, isi, thumbnail, sumber, highlight, kategori, dilihat, count(komen.id) as JmlKomen")
			->from('tbl_forum as forum')
			->join('tbl_forum_komentar as komen', "forum.id = komen.id_forum", "left")
			->where('highlight IS NOT NULL', NULL, FALSE)
			->group_by("forum.id, judul, tanggal_forum, isi, thumbnail, sumber, highlight, kategori")
			->order_by('tanggal_forum', 'DESC')
			->limit(3)
			->get();

		return $data->result();
	}
    function event() {
		$data = $this->db->select('judul, deskripsi, tanggal_mulai, tanggal_selesai')
			->from('tbl_event')
			->where("DATE(tanggal_mulai) =  CURDATE()", NULL, FALSE)
			->get();
			// var_dump($this->db->last_query());die();

		return $data->result();
	}
	function getCalendar($start, $end) {
		$data = $this->db->select("judul, deskripsi, tanggal_mulai, tanggal_selesai")
			->from("tbl_event")
			->get();
		
		return $data->result();
	}
}
?>

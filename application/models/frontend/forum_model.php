<?php
/*
 * Author : Arif Kurniawan
 * Email : arif.kurniawan86@gmail.com
 * Website : infoharga123.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class forum_model extends CI_Model
{
	var $table      = 'tbl_forum';
	var $table2		= 'tbl_forum_komentar';
    var $pk_name    = 'id';
    var $module     = '';
    function __construct(){
        parent::__construct();
    }
    function initialize($module) {
        if ( !empty($module) )
            $this->module = $module;
        else
            $this->module = $this->module;
	}
	
    function get_by_id($id = '') {
		$data = $this->db->select("*")
			->from($this->table)
			->where($this->pk_name, $id)
        	->get();

		return $data->row();
    }

    function get_komentar_by_id($id, $tipe) {
        $data = $this->db->select("*")
            ->from($this->table2)
            ->where("id_forum", $id)
            ->order_by("tanggal_komentar", "ASC")
            ->get();
        
        if($tipe == 'count') return $data->num_rows();
        else return $data->result();
    }

    function insert_komentar($data) {
        $data = $this->db->insert($this->table2, $data);

        return $data;
    }

    function update_total_viewer_by_id($id, $data) {
        $data = $this->db->where("id", $id)
            ->update($this->table, $data);

        return $data;
    }
}
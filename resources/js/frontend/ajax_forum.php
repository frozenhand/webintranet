<script type="text/javascript">
	var ForumTable = $('#list-data').dataTable({
		paging: true,
		lengthChange: false,
		searching: false,
		ordering: true,
		info: true,
		autoWidth: true,
		bFilter: false,
        // sDom: 'lrtip', // default is lfrtip, where the f is the filter
        language: {
			"sEmptyTable":	 "Tidak ada data yang tersedia pada tabel ini",
			"sProcessing":   "Sedang memproses...",
			"sLengthMenu":   "Tampilkan _MENU_ entri",
			"sZeroRecords":  "Tidak ditemukan data yang sesuai",
			"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
			"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
			"sInfoPostFix":  "",
			"sSearch":       "Cari:",
			"sUrl":          "",
        }
	});

	window.onload = function() {
		tampilForum();
	}

	function refreshForum() {
		ForumTable = $('#list-data').dataTable({
			paging: true,
			lengthChange: false,
			searching: false,
			ordering: true,
			info: true,
			autoWidth: true,
			bFilter: false,
	        // sDom: 'lrtip', // default is lfrtip, where the f is the filter
	        language: {
				"sEmptyTable":	 "Tidak ada data yang tersedia pada tabel ini",
				"sProcessing":   "Sedang memproses...",
				"sLengthMenu":   "Tampilkan _MENU_ entri",
				"sZeroRecords":  "Tidak ditemukan data yang sesuai",
				"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
				"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
				"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
				"sInfoPostFix":  "",
				"sSearch":       "Cari:",
				"sUrl":          "",
	        }
	    });
	}

	function tampilForum() {
        var id = $("#id_forum").val();
		$.get('<?php echo base_url('frontend/forum/tampil/'); ?>'+id, function(data) {
			ForumTable.fnDestroy();
			$('#forum-data').html(data);
			$('[data-toggle="tooltip"]').tooltip();
			refreshForum();
		});
    }
    
    $(document).on('submit', '#post-komentar', function(e){
		var formData = new FormData($(this)[0]);

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('frontend/forum/komentar'); ?>',
			data: formData,
			processData: false,
            contentType: false
		})
		.done(function(data) {
			// var out = jQuery.parseJSON(data);
			// if (out.status == 'form') {
			// 	$('.form-msg').html(out.msg);
			// 	effect_msg_form();
			// } else {
			// 	document.getElementById("form-update-usulanpengembangandiri").reset();
			// 	$('#update-usulanpengembangandiri').modal('hide');
			// 	$('.msg').html(out.msg);
			// 	effect_msg();
			// }
            tampilForum();
            $('#komentar').val('');
		})

		e.preventDefault();
	});
</script>